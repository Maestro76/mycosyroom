<!-- WRAP -->
<div class="wrap ctup">
<div class="slideup">
  <div class="container z-index100" style="background-color:#fff">
    <div class="slidercontainer">
      <div class="row" style="background-color:#fff">
        <div class="col-md-4 col-xs-12 go-right RTL_Bar" style="padding-right:0px;padding-left:0px">
          <ul class="nav nav-tabs nav-justified RTL pdr0">
            <?php  if(pt_main_module_available('hotels')){ ?>
            <li role="presentation" class="active text-center" data-title="HOTELS"> <a href="#HOTELS" data-toggle="tab" aria-controls="home" aria-expanded="true"><span class="hotel"></span> <?php echo trans('Hotels');?></a></li>
            <?php } ?>
            <?php  if(pt_main_module_available('ean')){ ?>
            <li role="presentation" class="text-center"  data-title="EXPEDIA"> <a href="#EXPEDIA" data-toggle="tab" aria-controls="home" aria-expanded="true"><i class="fa fa-bed"></i> <?php echo trans('Ean');?></a></li>
            <?php } ?>
            <?php  if(pt_main_module_available('flightsdohop')){ ?>
            <li role="presentation" class="text-center"  data-title="DOHOP"> <a href="#DOHOP" data-toggle="tab" aria-controls="home" aria-expanded="true"><span class="flight"></span> <?php echo trans('Flightsdohop');?></a></li>
            <?php } ?>
            <?php  if(pt_main_module_available('tours')){ ?>
            <li role="presentation" class="text-center" data-title="TOURS"> <a href="#TOURS" data-toggle="tab" aria-controls="home" aria-expanded="true"><span class="suitcase"></span> <?php echo trans('Tours');?></a></li>
            <?php } ?>
            <?php  if(pt_main_module_available('cars')){ ?>
            <li role="presentation" class="text-center" data-title="CARS"> <a href="#CARS" data-toggle="tab" aria-controls="home" aria-expanded="true"><span class="car"></span> <?php echo trans('Cars');?></a></li>
            <?php } ?>
            <?php  if(pt_main_module_available('cartrawler')){ ?>
            <li role="presentation" class="text-center" data-title="CARTRAWLER"> <a href="#CARTRAWLER" data-toggle="tab" aria-controls="home" aria-expanded="true"><span class="car"></span> <?php echo trans('Cars');?></a></li>
            <?php } ?>
          </ul>
          <div class="clearfix"></div>
          <div class="tab-content">
            <br><br>
            <div role="tabpanel" class="tab-pane fade active in <?php pt_searchbox('hotels'); ?>" id="HOTELS" aria-labelledby="home-tab">
              <?php require $themeurl.'views/hotels/main-search.php'; ?>
            </div>
            <div  role="tabpanel" class="tab-pane fade <?php pt_searchbox('ean'); ?>" id="EXPEDIA" aria-labelledby="home-tab">
              <?php require $themeurl.'views/integrations/ean/main-search.php'; ?>
            </div>
            <div  role="tabpanel" class="tab-pane fade <?php pt_searchbox('Flightsdohop'); ?>" id="DOHOP" aria-labelledby="home-tab">
              <?php require $themeurl.'views/integrations/flights/dohop/main_search.php'; ?>
            </div>
            <div  role="tabpanel" class="tab-pane fade <?php pt_searchbox('tours'); ?>" id="TOURS" aria-labelledby="home-tab">
              <?php require $themeurl.'views/tours/main-search.php'; ?>
            </div>
            <div  role="tabpanel" class="tab-pane fade <?php pt_searchbox('cars'); ?>" id="CARS" aria-labelledby="home-tab">
              <?php require $themeurl.'views/cars/main-search.php'; ?>
            </div>
            <div  role="tabpanel" class="tab-pane fade <?php pt_searchbox('cartrawler'); ?>" id="CARTRAWLER" aria-labelledby="home-tab">
              <?php require $themeurl.'views/integrations/cartrawler/main_search.php'; ?>
            </div>
          </div>
        </div>
        <div class="col-md-8 scolright go-left visible-lg visible-md">
          <div class="fullwidthbanner">
            <ul >
              <?php
                $mulcur = "";
                $mainslides = pt_get_main_slides();
                $scount = 0;
                foreach($mainslides as $ms){
                $sliderlib->set_id($ms->slide_id);
                $sliderlib->slide_details();
                $scount++;
                $sactive = "";
                if($scount == 1){
                $sactive = "active";
                }else{
                $sactive = "";
                }
                ?>
              <!-- FADE -->
              <li data-transition="fade" data-slotamount="1" data-masterspeed="300">
                <img src="<?php echo PT_SLIDER_IMAGES.$ms->slide_image;?>" alt="<?php echo $sliderlib->title;?>"/>
                <div class="tp-caption sft"
                  data-x="center"
                  data-y="100"
                  data-speed="1000"
                  data-start="800"
                  data-easing="easeOutExpo"  >
                  <div class="sboxpurple textcenter">
                    <span class="lato size28 slim caps white"><?php echo $sliderlib->title;?></span><br/><br/><br/>
                    <span class="lato size100 caps white"><?php echo $sliderlib->desc;?></span><br/>
                    <span class="lato size20 normal caps white"><?php echo trans('0273');?></span><br/><br/>
                    <span class="lato size48 slim uppercase yellow" style="text-shadow: 0 5px 7px rgba(9, 9, 15, 0.6);"><?php echo $ms->slide_optional_text;?></span><br/>
                  </div>
                </div>
              </li>
              <?php } ?>
            </ul>
            <div class="tp-bannertimer none"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var tpj=jQuery;
  //	tpj.noConflict();

  tpj(document).ready(function() {

  if (tpj.fn.cssOriginal!=undefined)
  	tpj.fn.css = tpj.fn.cssOriginal;

  	var api = tpj('.fullwidthbanner').revolution(
  		{
  			delay:9000,
  			startwidth:960,
  			startheight:446,

  			onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off

  			thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
  			thumbHeight:50,
  			thumbAmount:3,

  			hideThumbs:0,
  			navigationType:"none",				// bullet, thumb, none
  			navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

  			navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


  			navigationHAlign:"right",				// Vertical Align top,center,bottom
  			navigationVAlign:"bottom",					// Horizontal Align left,center,right
  			navigationHOffset:30,
  			navigationVOffset:30,

  			soloArrowLeftHalign:"left",
  			soloArrowLeftValign:"center",
  			soloArrowLeftHOffset:20,
  			soloArrowLeftVOffset:0,

  			soloArrowRightHalign:"right",
  			soloArrowRightValign:"center",
  			soloArrowRightHOffset:20,
  			soloArrowRightVOffset:0,

  			touchenabled:"on",						// Enable Swipe Function : on/off


  			stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
  			stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

  			hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
  			hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
  			hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


  			fullWidth:"on",

  			shadow:0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

  		});






  		// TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
  		// YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
  			api.bind("revolution.slide.onloaded",function (e) {


  				jQuery('.tparrows').each(function() {
  					var arrows=jQuery(this);

  					var timer = setInterval(function() {

  						if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
  						  arrows.fadeOut(500);
  					},3000);
  				})

  				jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
  					jQuery('.tp-simpleresponsive').addClass("mouseisover");
  					jQuery('body').find('.tparrows').each(function() {
  						jQuery(this).fadeIn(300);
  					});
  				}, function() {
  					if (!jQuery(this).hasClass("tparrows"))
  						jQuery('.tp-simpleresponsive').removeClass("mouseisover");
  				})
  			});
  		// END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS

  });




  jQuery(document).ready(function($){
  // gets the width of black bar at the bottom of the slider
  var $gwsr = $('.scolright').outerWidth();
  $('.blacklable').css({'width' : $gwsr +'px'});
  });
  jQuery(window).resize(function() {
  jQuery(document).ready(function($){

  	// gets the width of black bar at the bottom of the slider
  	var $gwsr = $('.scolright').outerWidth();
  	$('.blacklable').css({'width' : $gwsr +'px'});

  });
  });

</script>
<script>
  //------------------------------
  //CaroufredSell
  //------------------------------
  jQuery(document).ready(function(jQuery){
  "use strict";

  	jQuery("#foo2").carouFredSel({
  		width: "100%",
  		height: 240,
  		items: {
  			visible: 5,
  			minimum: 1,
  			start: 2
  		},
  		scroll: {
  			items: 1,
  			easing: "easeInOutQuad",
  			duration: 500,
  			pauseOnHover: true
  		},
  		auto: false,
  		prev: {
  			button: "#prev_btn2",
  			key: "left"
  		},
  		next: {
  			button: "#next_btn2",
  			key: "right"
  		},
  		swipe: true
  	});


  });


</script>