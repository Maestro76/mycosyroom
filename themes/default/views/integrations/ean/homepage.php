<?php  if(pt_main_module_available('ean')){ ?>
<div class="col-md-<?php echo $divCol; ?>">
<?php //print_r($popularHotelsEan);
  if(empty($featuredHotelsEan->errorMsg)){ ?>


<div class="lbl">
  <a href="<?php echo _BASE_URL_; ?>properties"><img src="<?php echo $theme_url; ?>images/featured-hotels.jpg" style="max-height:250px" class="fwimg"/></a>
  <div class="smallblacklabel go-text-right"><?php echo trans('013');?> <?php echo trans('Hotels');?></div>
</div>
<?php foreach($featuredHotelsEan->hotels as $item){ ?>

<div class="deal">
  <a href="<?php echo $item->slug;?>"><img src="<?php echo $item->thumbnail;?>" alt="<?php echo character_limiter($item->title,35);?>" style="height:50px;width:80px"class="dealthumb go-right"/></a>
  <div class="dealtitle go-right">
    <p><a href="<?php echo $item->slug;?>" class="dark go-text-right go-right rtl_title_home"> <?php echo character_limiter($item->title,35);?></a>
</p>
    <span class="size13 grey mt-9 go-right"><?php echo $item->stars;?> <span class="go-left"><?php echo $item->location;?></span>&nbsp;</span>
  </div>
  <div class="dealprice go-left">
    <?php  if($item->price > 0){ ?>
    <p class="size12 grey lh2"><?php echo $item->currCode;?><span class="price">
      <?php echo $item->currSymbol; ?><?php echo $item->price;?>
      <?php } ?>
      </span>
    </p>

  </div>
</div>
<?php } ?>
<div class="clearfix"></div>

<?php } ?>
<?php if(!empty($popularHotels)){ ?>
<?php } ?>
<a href="<?php echo _BASE_URL_; ?>properties" class="btn iosbtn btn-block"><?php echo trans('0185');?> <?php echo trans('Hotels');?></a>
<br><br>
</div>
<?php } ?>