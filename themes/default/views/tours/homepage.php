<?php  if(pt_main_module_available('tours')){ ?>
  <div class="col-md-<?php echo $divCol; ?>">
<div class="lbl">
  <a href="<?php echo _BASE_URL_; ?>tours"><img src="<?php echo $theme_url; ?>images/featured-tours.jpg" alt="" class="fwimg"/></a>
  <div class="smallblacklabel go-text-right"><?php echo trans('013');?> <?php echo trans('Tours');?></div>
</div>
<?php if(!empty($featuredTours) || !empty($popularTours)){ ?>
<div id="pop-tours" class="panel-collapse collapse" style="background-color:#F4F3F1">
  <div class="modal-header">
    <button type="button" class="close" data-toggle="collapse" data-parent="#accordion" href="#pop-tours">&times;</button>
    <h4 class="modal-title"><?php echo trans('0452');?></h4>
  </div>
  <br>
  <?php foreach($popularTours as $item){ ?>
  <div class="deal">
    <a href="<?php echo $item->slug;?>"><img src="<?php echo $item->thumbnail;?>" alt="<?php echo character_limiter($item->title,15);?>" style="height:50px;width:80px"class="dealthumb"/></a>
    <div class="dealtitle">
      <p><a href="<?php echo $item->slug;?>" class="dark"><?php echo character_limiter($item->title,15);?></a></p>
      <span class="size13 grey mt-9"><?php echo $item->stars;?> <?php echo $item->location;?></span>
    </div>
    <div class="dealprice">
      <?php  if($item->price > 0){ ?>
      <p class="size12 grey lh2"><?php echo $item->currCode;?><span class="price">
        <?php echo $item->currSymbol; ?><?php echo $item->price;?>
        <?php } ?>
        </span>
      </p>
      <!--
        <?php if($item->avgReviews->overall > 0){ ?>
        <div id="score"><span><?php echo $item->avgReviews->overall;?></span></div>
        <?php } ?>

        <?php echo wishListInfo("tours", $item->id); ?>
        -->
    </div>
  </div>
  <?php } ?>
  <div class="clearfix">
  </div>
</div>
<?php foreach($featuredTours as $item){ ?>
<div class="deal">
  <a href="<?php echo $item->slug;?>"><img src="<?php echo $item->thumbnail;?>" alt="<?php echo character_limiter($item->title,35);?>" style="height:50px;width:80px"class="dealthumb go-right"/></a>
  <div class="dealtitle go-right">
    <p><a href="<?php echo $item->slug;?>" class="dark go-text-right go-right rtl_title_home"> <?php echo character_limiter($item->title,35);?></a>
</p>
    <span class="size13 grey mt-9 go-right"><?php echo $item->stars;?> <span class="go-left"><?php echo $item->location;?></span>&nbsp;</span>
  </div>
  <div class="dealprice go-left">
    <?php  if($item->price > 0){ ?>
    <p class="size12 grey lh2"><?php echo $item->currCode;?><span class="price">
      <?php echo $item->currSymbol; ?><?php echo $item->price;?>
      <?php } ?>
      </span>
    </p>
    <!--
      <?php if($item->avgReviews->overall > 0){ ?>
      <div id="score"><span><?php echo $item->avgReviews->overall;?></span></div>
      <?php } ?>

      <?php echo wishListInfo("cars", $item->id); ?>
      -->
  </div>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php } ?>

<?php if(!empty($popularTours)){ ?>
<!--<button data-toggle="collapse" data-parent="#accordion" href="#pop-tours" class="btn iosbtn btn-block"><?php echo trans('0452');?></button>
  --><?php } ?>
<a href="<?php echo _BASE_URL_; ?>tours" class="btn iosbtn btn-block"><?php echo trans('0185');?> <?php echo trans('Tours');?></a>
<br><br>
</div>
<?php } ?>