<?php
header('Content-type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8" ?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo _BASE_URL_;?></loc>
        <priority>1</priority>
    </url>

 <?php 
if(!empty($cms)){
 foreach($cms as $url) { ?>
    <url>
        <loc><?php echo _BASE_URL_.$url->page_slug; ?></loc>
        <priority>1</priority>
    </url>
    <?php } }
    if(!empty($hotels)){
    foreach($hotels as $h) { ?>
    <url>
        <loc><?php echo _BASE_URL_."hotels/".$h->hotel_slug; ?></loc>
 <description>   <![CDATA[   <?php echo $h->hotel_meta_desc; ?>]]></description>
        <priority>1</priority>
    </url>
    <?php } }
    if(!empty($cruises)){
     foreach($cruises as $cruise) { ?>
    <url>
        <loc><?php echo _BASE_URL_."cruises/".$cruise->cruise_slug; ?></loc>
 <description>   <![CDATA[   <?php echo $cruise->cruise_meta_desc; ?>]]></description>
        <priority>1</priority>
    </url>
    <?php } }
    if(!empty($cars)){
    foreach($cars as $car) { ?>
    <url>
        <loc><?php echo _BASE_URL_."cruises/".$car->car_slug; ?></loc>
 <description>   <![CDATA[   <?php echo $car->car_meta_desc; ?>]]></description>
        <priority>1</priority>
    </url>
    <?php } }
    if(!empty($tours)){
    foreach($tours as $tslug) { ?>
    <url>
        <loc><?php echo _BASE_URL_."tours/".$tslug->tour_slug; ?></loc>
       <description>   <![CDATA[   <?php echo $tslug->tour_meta_desc; ?>]]></description>
        <priority>1</priority>
    </url>
    <?php } } ?>


</urlset>
