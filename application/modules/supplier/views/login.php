<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/> 
    <link rel="shortcut icon" href="<?php echo PT_GLOBAL_IMAGES_FOLDER . 'favicon.png'; ?> ">
    <title><?php echo $pagetitle; ?></title>
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/tabs.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/mobile.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/global.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/fontello.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/font-awesome.min.css" />
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700italic,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/jquery.fancybox.css?v=2.1.5" media="screen" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/jquery-ui.css" />
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/jquery.selectbox-0.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/pi.tab.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/pi.init.tab.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/supplier/js/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>assets/include/login/spin.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/include/login/ladda.min.js"></script>
</head>
<body>
<style>
#rotatingDiv {
        display: block;
        margin: 32px auto;
        height: 100px;
        width: 100px;
        -webkit-animation: rotation .9s infinite linear;
        -moz-animation: rotation .9s infinite linear;
        -o-animation: rotation .9s infinite linear;
        animation: rotation .9s infinite linear;
        border-left: 8px solid rgba(0, 0, 0, .20);
        border-right: 8px solid rgba(0, 0, 0, .20);
        border-bottom: 8px solid rgba(0, 0, 0, .20);
        border-top: 8px solid rgba(33, 128, 192, 1);
        border-radius: 100%;
    }

    @keyframes rotation {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(359deg);
        }
    }

    @-webkit-keyframes rotation {
        from {
            -webkit-transform: rotate(0deg);
        }
        to {
            -webkit-transform: rotate(359deg);
        }
    }

    @-moz-keyframes rotation {
        from {
            -moz-transform: rotate(0deg);
        }
        to {
            -moz-transform: rotate(359deg);
        }
    }

    @-o-keyframes rotation {
        from {
            -o-transform: rotate(0deg);
        }
        to {
            -o-transform: rotate(359deg);
        }
    }
</style>
<script>
    $(document).ready(function() {
        $("#btn-register-form").click(function() {
          $(".form-signup").show();
          $(".form-signin").hide();
        });
        $(".form-signup .btn-back").click(function() {
          $(".form-signup").hide();
          $(".form-signin").show();
        });
        $("#link-forgot").click(function() {
          $(".largeLoginBox").show();
          $(".loginBox").hide();
        });
        $(".form-forgot .btn-back").click(function() {
          $(".largeLoginBox").hide();
          $(".loginBox").show();
        });    
    });
    
</script>
<script type="text/javascript">
    $(function () {
        //login functionality
        $(".form-signin").on('submit', function () {
            $(".resultlogin").html("<div class='alert alert-info loading wow fadeOut animated'>Hold On...</div>");
            $.post("<?php echo _BASE_URL_ .$this->uri->segment(1);?>/login", $(".form-signin").serialize(), function (response) {
                var resp = $.parseJSON(response);
                if (!resp.status) {
                    $(".resultlogin").html("<div class='alert alert-danger loading wow fadeIn animated'>" + resp.msg + "</div>");
                } else {
                    $(".resultlogin").html("<div class='alert alert-success login wow fadeIn animated'>Redirecting Please Wait...</div>");
                    window.location.replace(resp.url);
                }

            });
        });
        // end login functionality
        // start password reset functionality
        $(".resetbtn").on('click', function () {
            var resetemail = $("#resetemail").val();
            
            if (resetemail == "") {
                alert("Please Enter Email Address");
            } else {
                $(".resultreset").html("<div id='rotatingDiv'></div>");
                $.post("<?php echo _BASE_URL_. $this->uri->segment(1);?>/resetpass", $("#passresetfrm").serialize(), function (response) {
                    alert(resetemail);
                    if ($.trim(response) == '1') {
                        $(".resultreset").html("<div class='alert alert-success'>New Password sent to " + resetemail + ", Kindly check email.</div>");

                    } else {
                        $(".resultreset").html("<div class='alert alert-danger'>Email Not Found</div>");

                    }
                });
            }
        });
        // end password reset functionality


    });
</script>
    <div class="header_wrapper">
    	<div class="pi-section pi-no-padding">
        	<div class="headerContent">
                <span class="not_register_text">Vous n'avez pas encore de compte?</span>
                <a href="#" class="register_bouton purple_gradient button"><span>Inscription</span></a>
            </div>
        </div>
    </div>
    <div class="loginBox  wow flipInX animated" style="display: block;" >
    	<div class="logoLogin">
        	<a href="#" title="My-cosyroom"><img src="<?php echo base_url(); ?>assets/supplier/images/logo.png" alt="My-cosyroom" /></a>
        </div>
        <div class="login_form">
        	<form class="form-signin" method="POST" role="form" onsubmit="return false;">
            	<div class="logForm">
                	<label class="log_lib">Identifiant : </label>
            		<input type="text" class="input_log"  required="" autofocus="" name="email"  />
                    <div class="clear"></div>
                </div>
                <div class="logForm">
                	<label class="log_lib">Mot de passe :</label>
            		<input type="password" name="password"   required=""class="input_log" />
                    <div class="clear"></div>
                </div>
                <div class="logForm actionLog">
                    <button class="connexion_bouton purple_gradient button ladda-button"><i class="icon-paper-plane"></i> Envoyer</button>
                    <a class="mdp_lost" id="link-forgot" title="Mot de passe oubli&eacute;" href="#">Mot de passe oubli&eacute;</a>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
        <div class="resultlogin"></div>
        <div class="text_incription">Si vous ne diposez pas de compte vous pouvez <a href="#" title="vous inscrire">vous inscrire</a></div>
    </div>
    <div class="largeLoginBox  wow flipInY animated" style="display: none;" >
    	<div class="logoLogin">
        	<a href="#" title="My-cosyroom"><img src="<?php echo base_url(); ?>assets/supplier/images/logo.png" alt="My-cosyroom" /></a>
        </div>
        <div class="login_form">
        	<div class="popBox_text">Veuillez saisir votre identifiant pourrécupérer votre mot de passe:</div>
        	<form class="form-forgot" onsubmit="return false;" id="passresetfrm">
                <div class="resultreset"></div>
            	<div class="logForm">
                	<label class="log_lib">Identifiant:</label>
            		<input type="email"  id="resetemail" name="email" class="input_log" />
                    <div class="clear"></div>
                </div>
                <div class="logForm actionLog">
                    <button type="button" class="btn ladda-button ladda-button btn-back" style="padding: 15px 12px;"><i class="fa fa-angle-left"></i>&nbsp;Back</button>
                    <button id="btn-forgot" class="connexion_bouton purple_gradient button resetbtn ladda-button" ><i class="icon-check"></i> Valider</button>
                    <div class="clear"></div>
                </div>
                <div class="text_incription">Si vous ne diposez pas de compte vous pouvez <a href="#" title="vous inscrire">vous inscrire</a></div>
            </form>
        </div>
    </div>
    
    <div class="footer_login">
        <span class="copyright_text">Copyright &copy; 2015 - my-cosyroom</span>
    </div>
</body>
 <script>
    // Bind normal buttons
    Ladda.bind('div:not(.progress-demo) button', { timeout: 2000 });

    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
        callback: function (instance) {
            var progress = 0;
            var interval = setInterval(function () {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);
                if (progress === 1) {
                    instance.stop();
                    clearInterval(interval);
                }
            }, 200);
        }
    });
</script>
