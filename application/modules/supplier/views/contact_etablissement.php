<!-- Right Column -->
                <div class="pi-col-sm-8">
                	<div class="contenu_page">
                    	<h1 class="titre_page">Contact de l’établissement</h1>
                        <div class="advertisment_message">
                        	Vous ne pouvez pas modifier directement vos informations. Vous devez contacter un account manager de my-cosyroom à l'adresse contact@my-cosyroom.com ou cliquer sur le bouton <strong>"Demande modification profil"</strong>
                        </div>
                        <div class="formContent">
                        	<form method="POST">
                            	<div class="pi-row">
                                <div class="pi-col-sm-12">
                                    <div class="titre_form"><span>Votre hotel est en ligne depuis : 14 - 04 - 2015</span></div>
                                </div>
                                <div class="pi-col-sm-12">
                                    <div class="formBox">
                                        <label class="form_et">Civilité :</label>
                                        <div class="choiceBox">
                                        	<label><input type="radio" class="checkStyle" name="civilite" checked="checked" /><span>Mr</span><div class="clear"></div></label>
                                           <label><input type="radio" class="checkStyle" name="civilite" /><span>Mme</span><div class="clear"></div></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Votre prénom :</label>
                                        <input type="text" name="fname" class="input_txt" value="<?php echo $profile[0]->ai_first_name;?>"  />
                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Votre fonction :</label>
                                        <input type="text" class="input_txt" value="Reception"  />
                                    </div>
                                </div>
                               
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Votre nom :</label>
                                        <input type="text" name="lname"  class="input_txt" value="<?php echo $profile[0]->ai_last_name;?>"  />
                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Raison sociale :</label>
                                        <input type="text" class="input_txt" value="10000000000"  />
                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Votre téléphone :</label>
                                        <input type="text" name="mobile" class="input_txt" value="<?php echo $profile[0]->ai_mobile;?>"  />
                                    </div>
                                </div>
                                 <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et"> N° TVA :</label>
                                        <input type="text" class="input_txt" value="004859"  />
                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Votre email :</label>
                                        <input type="text" disabled="disabled" class="input_txt" value="<?php echo $profile[0]->accounts_email;?>"  />

                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Choix du pack:</label>
                                        <div class="selectBox">
                                            <select class="selectStyled">
                                                <option>Basique</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <div class="pi-col-sm-12">
                                    <div class="titre_form"><span>Afin d’améliorer nos services, dites-en nous plus sur vos outils</span></div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Moteur de réservation : :</label>
                                        <div class="selectBox">
                                            <select class="selectStyled">
                                                <option>Faites votre choix</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">Channel manager :</label>
                                        <div class="selectBox">
                                            <select class="selectStyled">
                                                <option>Faites votre choix</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="pi-col-sm-6">
                                    <div class="formBox">
                                        <label class="form_et">PMS :</label>
                                         <select class="selectStyled">
											<option>PMS</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
										</select>

                                    </div>
                                </div>
                                <div class="pi-col-sm-12">
                                    <div class="message_form">Toutes les modifications sont soumises pour validation à my-cosyroom avant mise en ligne</div>
                                    <div class="bouton_form">
                                        <input type="hidden" name="update" value="1" />
                                         <input type="hidden" name="type" value="<?php echo $profile[0]->accounts_type;?>" />
                                         <input type="hidden" name="status" value="<?php echo $profile[0]->accounts_status;?>" />
                                         <input type="hidden" name="oldemail" value="<?php echo $profile[0]->accounts_email;?>" />
                                        <button type="submit" class="purple_gradient button"><span>Enregistrer</span></button>
                                    </div>
                                </div>
                             </div>
                             
                         </form>
                        </div>
                    </div>
                </div>