<!-- Right Column -->
<div class="pi-col-sm-8">
	<div class="contenu_page">
    	<h1 class="titre_page">Mon compte</h1>
        <div class="advertisment_message">
        	Vous ne pouvez pas modifier directement vos informations. Vous devez contacter un account manager de my-cosyroom à l'adresse contact@my-cosyroom.com ou cliquer sur le bouton <strong>"Demande modification profil"</strong>
        </div>
        <div class="pi-row pi-no-margin-right">
        	<div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_1">
                	<a href="<?php echo _BASE_URL_.$this->uri->segment(1);?>/contact_etablissement" title="">
                        <strong class="menuHome_lib">Contact de l’établissement</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_2">
                    <a href="<?php echo _BASE_URL_.$this->uri->segment(1);?>/informations" title="">
                        <strong class="menuHome_lib">Informations générales</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_3">
                	<a href="#" title="">
                        <strong class="menuHome_lib">Caract&eacute;ristiques générales</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_4">
                    <a href="#" title="">
                        <strong class="menuHome_lib">Galerie photos</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_5">
                	<a href="#" title="">
                        <strong class="menuHome_lib">Moteur de réservation</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_dispo">
                	<a href="#" title="">
                        <strong class="menuHome_lib">Tarifs et disponibilités</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_reserv">
                	<a href="#" title="">
                        <strong class="menuHome_lib">Réservations</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_7">
                    <a href="#" title="">
                        <strong class="menuHome_lib">Avis</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_7">
                    <a href="#" title="">
                        <strong class="menuHome_lib">Statistiques</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
            <div class="pi-col-md-6 pi-no-padding-right">
                <div class="menuHome menu_home_8">
                    <a href="#" title="">
                        <strong class="menuHome_lib">Mot de passe</strong>
                        <span class="menuHome_text">Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus patens, quam plagam Nicator Seleucus.</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>