<script>
    $(document).ready(function() {
        $('#edit_map').click(function(e) {
            e.preventDefault();
            var longitude ,latitude;
            longitude = $("input[name='longitude']").val();
            latitude = $("input[name='latitude']").val();
            var src = 'https://www.google.com/maps/embed/v1/view?key=AIzaSyDjYtiFxvd8pVWKkRBS_Myt2UrDpY2dXV0&zoom=5'
                +'&center='+longitude+','+latitude;
            $('.map_info iframe').attr('src',src);
        });
    });
</script>
<!-- Right Column -->
                <div class="pi-col-sm-8">
                	<div class="contenu_page">
                    	<h1 class="titre_page">Informations générales de votre établissement</h1>
                        <div class="advertisment_message">
                        	Vous ne pouvez pas modifier directement vos informations. Vous devez contacter un account manager de my-cosyroom à l'adresse contact@my-cosyroom.com ou cliquer sur le bouton <strong>"Demande modification profil"</strong>
                        </div>
                        <div class="map_info">
                        	<iframe
                              width="100%"
                              height="450"
                              frameborder="0" style="border:0"
                              src="https://www.google.com/maps/embed/v1/view?key=AIzaSyDjYtiFxvd8pVWKkRBS_Myt2UrDpY2dXV0
                                &center=48.7733462,2.7642218
                              &zoom=10" >
                            </iframe>

                        </div>
                        <div class="formContent">
                        	<form>
                            	<div class="pi-row">
                                	<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Nom de l’établissement :</label>
                                            <input required="" type="text" class="input_txt" name="hotel_name" value="Hotel High Five Palace par exemple"  />
                                        </div>
                                    </div>
                                    <div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Type hébergement: </label>
											<select class="selectStyled" name="hotel_type">
												<option>Rue Boxart Trade Le Moulin</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
											</select>
                                          </div>
                                     </div>
                                    <div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Code postal :</label>
                                            <input  required="" type="text" class="input_txt" name="hotel_postal" value="41523" />
                                        </div>
                                    </div>
                                    <div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Ville :</label>
                                            <select  name="hotel_ville" class="selectStyled">
												<option>Ville test</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="pi-col-sm-12">
                                    	<div class="formBox">
                                        	<label class="form_et">Adresse hotel :</label>
                                            <input type="text" name="hotel_address" required="" class="input_txt" value="Rue Boxart Trade Le Moulin" />
                                        </div>
                                    </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Région: </label>
											<select class="selectStyled" name="hotel_region">
												<option>Rue Boxart Trade Le Moulin</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
											</select>
                                        </div>
                                     </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Pays: </label>
											<select name="hotel_pays" class="selectStyled">
												<option value="france">France</option>
												<option value="france">Lorem ipsum</option>
												<option value="france">Lorem ipsum</option>
												<option value="france">Lorem ipsum</option>
											</select>
                                        </div>
                                    </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Devise: </label>
											<select class="selectStyled" name="hotel_devise">
												<option value="eur">Euro</option>
												<option value="usd">USD</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
											</select>
                                        </div>
                                    </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Classification:  </label>
											<select name="hotel_classification" class="selectStyled">
												<option>Euro</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
												<option>Lorem ipsum</option>
											</select>
                                        </div>
                                    </div>
									<div class="pi-col-sm-12">
										<label>Editer les coordonnées géographiques; <a id="edit_map" href="#" style="display:inline-block; color:#1b1b1b; text-decoration:underline">Cliquez ici</a></label>
									</div>
                                    
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Longitude :</label>
                                            <input type="text" name="hotel_longitude" class="input_txt" value="48.7733462" />
                                        </div>
                                    </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Latitude :</label>
                                            <input type="text" name="hotel_latitude" class="input_txt" value="2.7642218" />
                                        </div>
                                    </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Email de réservation :</label>
                                            <input type="email"  required="" name="hotel_email"class="input_txt" value="loremipsum@test.com" />
                                        </div>
                                    </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        <label class="form_et"><br /></label>
                                        	<label class="form_et">(Nous vous envoyons les confirmations de réservations à cette adresse mail)</label>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Téléphone réception/ réservation :</label>
                                            <input type="text" required="" name="hotel_phone" class="input_txt" value="Lroem" />
                                        </div>
                                    </div>
									<div class="pi-col-sm-6">
                                    	<div class="formBox">
                                        	<label class="form_et">Fax :</label>
                                            <input name="hotel_fax" type="text" class="input_txt" value="Lorem" />
                                        </div>
                                    </div>
                                    <div class="pi-col-sm-12">
										<label><strong>Officiel site</strong> : </label>
                                        <div class="clear"></div>
										<div class="pi-col-sm-6 pi-no-padding-left">
                                        <input type="text" class="input_txt" name="hotel_site" value="Lorem" /> <font class="or_absolute">ou</font></div>
                                        <div class="pi-col-sm-6">
											<div class="checkBloc">
												<label>
													<input type="checkbox" class="checkStyle" checked="checked" />
													<span>L’hotel ne possède pas de siteweb</span>
													<div class="clear"></div>
												</label>
											</div>
										</div><div class="clear"></div>
                                    </div>
									<div class="pi-col-sm-12">
                                    	<div class="formBox">
                                        	<label class="form_et">Description hotel :</label>
                                            <textarea name="hotel_desc" class="textarea_txt"  onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''">Merci de rédiger ou copier/coller une courte description sur votre établissement et ses environs.</textarea>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="pi-col-sm-12">
                                    	<div class="titre_form"><span>CONDITIONS SPECIFIQUES DE VOTRE ETABLISSEMENT</span></div>
                                    </div>
                                    <div class="pi-col-sm-6">
                                    	<div class="sous_titre_form">Heure de Check in</div>
                                    	<div class="pi-row">
                                            <div class="pi-col-sm-6">
                                                <div class="formBox">
                                                    <div class="selectBox">
                                                        <select class="selectStyled" name="hotel_checkin1">
                                                            <option>00h00</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pi-col-sm-6">
                                                <div class="formBox">
                                                    <div class="selectBox">
                                                        <select class="selectStyled" name="hotel_checkin2">
                                                            <option>00h00</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pi-col-sm-6">
                                    	<div class="sous_titre_form">Heure de Check out</div>
                                    	<div class="pi-row">
                                            <div class="pi-col-sm-6">
                                                <div class="formBox">
                                                    <div class="selectBox">
                                                        <select class="selectStyled" name="hotel_out1">
                                                            <option>00h00</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pi-col-sm-6">
                                                <div class="formBox">
                                                    <div class="selectBox">
                                                        <select class="selectStyled" name="hotel_out2">
                                                            <option>00h00</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                            <option>Lorem ipsum</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="pi-col-sm-12">
                                    	<div class="titre_form"><span>Politique enfants</span></div>
                                    </div>
                                    <div class="pi-col-sm-4">
                                        <div class="choiceBox">
                                            <label><input type="radio" class="checkStyle" name="hotel_civilite" checked="checked" /><span>Gratuit</span><div class="clear"></div></label><br />
                                           <label><input type="radio" class="checkStyle" name="hotel_civilite" /><span>Aucune gratuité</span><div class="clear"></div></label>
                                        </div>    
                                    </div>
                                    <div class="pi-col-sm-8">
                                    	<div class="pi-col-sm-6">
                                            <div class="formBox form_inline">
                                                <label class="form_cote">DE:</label>
                                                <div class="selectBox">
                                                    <select class="selectStyled">
                                                        <option>0</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pi-col-sm-6">
                                            <div class="formBox form_inline">
                                                <label class="form_cote">A :</label>
                                                <div class="selectBox">
                                                    <select class="selectStyled">
                                                        <option>1</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                        <option>8</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    	<div class="clear"></div>    
                                    </div>
                                    <div class="clear"></div>
                                    <div class="pi-col-sm-12">
                                    	<div class="formBox">
                                        	<label class="form_et">Informations importantes:</label>
                                            <textarea  name="hotel_importantes" class="textarea_txt"  onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''">
Merci de renseigner les informations importantes de votre établissement
Exemple:
En cas d'arrivée tardive le jour de votre check-in(Après les heures d'ouvertures) , vous devez impérativement contacter l'hôtel.
Pour nous localiser plus facilement, entrer dans votre GPS,la rue de Paris.
Pour toute réservation supérieure à 5 chambres, des conditions et frais supplémentaires peuvent être appliqués</textarea>
                                        </div>
                                    </div>
                                    <div class="pi-col-sm-12">
                                    	<div class="message_form">Toutes les modifications sont soumises pour validation à my-cosyroom avant mise en ligne</div>
                                        <div class="bouton_form">
                                            <input type="hidden" name="update" value="1" />
                                        	<button type="submit" class="purple_gradient button"><span>Enregistrer</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>