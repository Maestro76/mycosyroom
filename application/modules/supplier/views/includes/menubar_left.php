                <!-- Left Menu Column -->
                <div class="pi-col-sm-4">
                	<div class="menuLeftBox">
                    	<div class="logo"><a href="<?php echo _BASE_URL_.$this->uri->segment(1);?>" title="My-cosyroom"><img src="<?=base_url()?>assets/supplier/images/logo_home.png" alt="My-cosyroom" /></a></div>
                        <div class="navMobile">
                        	<div class="nomHotelMobile">
                                <a href="#" class="thumbHotel">
                                    <img src="<?=base_url()?>assets/supplier/images/thumbHome.jpg" alt="" />
                                    <span><?php echo $this->session->userdata('fullName');?></span>
                                    <div class="clear"></div>
                                </a>
                            </div>
                            <div class="navButton"><a href="#" title=""><i class="icon-list"></i></a></div>
                            <div class="logoutButton"><a href="#" title=""><i class="icon-logout"></i></a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="menuList">
                            <ul class="onglets">
                                <li class="navItem"><a href="<?php echo _BASE_URL_.$this->uri->segment(1);?>/contact_etablissement" class="navLink" title="Contact de l'établissement"><span class="ico_home">Contact de l'établissement</span></a></li>
                                <li class="navItem"><a href="<?php echo _BASE_URL_.$this->uri->segment(1);?>/informations" class="navLink" title="Informations générales"><span class="ico_info">Informations générales</span></a></li>
								<li class="navItem"><a href="#" class="navLink" title="Caracteristiques"><span class="ico_list">Caractéristiques générales</span></a></li>
                                <li class="navItem"><a href="#" class="navLink" title="Galerie photo"><span class="ico_photo">Galerie photos</span></a></li>
                                <li class="navItem"><a href="#" class="navLink" title="Moteur de réservation"><span class="ico_setting">Moteur de réservation</span></a></li>
                                <li class="navItem"><a href="#" class="navLink" title="Moteur de réservation"><span class="ico_tarif">Tarifs et disponibilités</span></a></li>
                                <li class="navItem"><a href="#" class="navLink" title="Réservations"><span class="ico_reservation">Réservations</span></a></li>
								<li class="navItem"><a href="#" class="navLink" title="Avis"><span class="ico_avis">Avis</span></a></li>
                                <li class="navItem"><a href="#" class="navLink" title="Facture"><span class="ico_tag">Factures</span></a></li>
                                <li class="navItem"><a href="#" class="navLink" title="Statistiques"><span class="ico_stat">Statistiques</span></a></li>
                                <li class="navItem"><a href="#" class="navLink" title="Mot de passe"><span class="ico_lock">Mot de passe</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="box_left">
                    	<div class="helpBox">
                        	<div class="helpBox_lib">Besoin d'aide ?</div>
                            <div class="helpBox_button"><a href="#" class="purple_gradient button">Contactez-nous</a></div>
                        </div>
                    </div>
                    <div class="box_left">
                    	<div class="demandeBox">
                        	<div class="demandeContent">
                            	<div class="demande_lib">Demande de modification du profil</div>
                                <div class="demande_button"><a href="#" class="purple_gradient button">Envoyer la demande</a></div>
                            </div>
                        </div>
                    </div>
                </div>