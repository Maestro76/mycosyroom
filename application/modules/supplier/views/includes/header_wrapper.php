<div class="header_wrapper">
	<div class="pi-section pi-no-padding">
    	<div class="headerContainer">
            <div class="welcomeText">Bienvenue dans votre extranet</div>
            <div class="nomHotel">
            	<a href="<?php echo _BASE_URL_.$this->uri->segment(1);?>/profile/" class="thumbHotel">
                	<img src="<?php echo base_url(); ?>assets/supplier/images/thumbHome.jpg" alt="" />
                    <span><?php echo $this->session->userdata('fullName');?></span>
                    <div class="clear"></div>
                </a>
            </div>
            <div class="logOut"><a href="<?php echo _BASE_URL_.$this->uri->segment(1);?>/logout" class="D&eacute;connexion">D&eacute;connexion</a></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>