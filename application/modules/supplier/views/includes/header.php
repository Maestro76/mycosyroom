<!DOCTYPE html>

<!--
Product:        PHPTRAVELS
Copyright:      2015 @ phptravels.com
License:        http://phptravels.com/licenses
Purchase:       http://phptravels.com/order
-->

<!-- Pace -->
<script src="<?php echo base_url(); ?>assets/include/pace/pace.min.js"></script>
<link href="<?php echo base_url(); ?>assets/include/pace/dataurl.css" rel="stylesheet" />
<!-- Pace -->

<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<base href="<?php echo base_url(); ?>" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/global/favicon.png">
<link href="<?php echo base_url(); ?>assets/include/loading/loading.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/admin.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/tabs.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/mobile.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/global.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/fontello.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/font-awesome.min.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700italic,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">
<?php if(empty($dontload)){ ?>
<script src="<?php echo base_url(); ?>assets/include/ckeditor/ckeditor.js"></script><?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/supplier/css/jquery-ui.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/jquery.selectbox-0.2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/scripts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/pi.tab.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/supplier/js/pi.init.tab.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/supplier/js/jquery-ui.js"></script>
    

<link href="<?php echo base_url(); ?>assets/include/alert/css/alert.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/include/alert/themes/default/theme.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/include/alert/js/alert.js"></script>

<!---Jquery Form--->
<script src="<?php echo base_url();?>assets/include/jquery-form/jquery.form.min.js"></script>

<style>
.wrapper .main { margin-top: 55px; } @media screen and (max-width: 480px) { .wrapper .main { margin-top: 100px;  }  }
</style>

</head>

<body>
<?php include 'header_wrapper.php'; ?>
<div class="pi-section pi-no-padding">
<div class="page">
<div class="pi-row">
<?php include 'menubar_left.php'; ?>


