<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

# Mano Rakotondrasata
# <rakotondrasataandriamanohisoa@gmail.com>
# Ajout de fonctionnalité multi-recherche

if ( ! function_exists('p'))
{
    function p($object){
        echo "<pre>" ;
        print_r($object) ;
        echo "</pre>" ;
    }  
}

if ( ! function_exists('d'))
{
    function d($object){
        echo "<pre>" ;
        print_r($object) ;
        echo "</pre>" ;
        exit() ;
    }  
}
