﻿<?php

/* Translate only words on right side under "" */

/* Modules Names */

$L1["Hotels"]       =  "Hoteles   ";
$L1["Tours"]        =  "Tours     ";
$L1["Offers"]       =  "Ofertas   ";
$L1["Ean"]          =  "Hoteles   ";
$L1["Flightsdohop"] =  "Vuelos    ";
$L1["Cars"]         =  "Cars      ";
$L1["Blog"]         =  "Blog      ";
$L1["Cartrawler"]   =  "Cars      ";

/* Translated Words */

$L1["01"] = "Inicio";
$L1["02"] = "Cuenta";
$L1["03"] = "Cerrar sesión";
$L1["04"] = "Login";
$L1["05"] = "Registro";
$L1["06"] = "Encontrar qué?";
$L1["07"] = "El registro";
$L1["08"] = "fecha";
$L1["09"] = "Confirmar";
$L1["010"] = "adultos";
$L1["011"] = "Niño";
$L1["012"] = "Buscar";
$L1["013"] = "destacado";
$L1["014"] = "Hoteles populares";
$L1["015"] = "Últimos Hotels";
$L1["016"] = "Rooms";
$L1["017"] = "Fotos";
$L1["018"] = "Videos";
$L1["019"] = "Puntuación de los comentarios";
$L1["020"] = "Seguimiento del pedido";
$L1["021"] = "ID";
$L1["022"] = "Ok";
$L1["023"] = "Newsletter";
$L1["024"] = "Únete a nuestro boletín de noticias para mantener a ser informado sobre ofertas y novedades.";
$L1["025"] = "Regístrate ahora";
$L1["026"] = "si reserva";
$L1["027"] = "Comprobar disponibilidad";
$L1["028"] = "Eliminar de la lista de deseos";
$L1["029"] = "Añadir a la lista de deseos";
$L1["030"] = "limpio";
$L1["031"] = "Comfort";
$L1["032"] = "Lugar";
$L1["033"] = "Instalaciones";
$L1["034"] = "personal";
$L1["035"] = "Evaluación de los clientes";
$L1["036"] = "por";
$L1["037"] = "Personas";
$L1["038"] = "Compartir";
$L1["039"] = "Resumen";
$L1["040"] = "Políticas";
$L1["041"] = "Callejero";
$L1["042"] = "Comentarios";
$L1["043"] = "Comentarios";
$L1["044"] = "Galería";
$L1["045"] = "Por favor, lea los términos y política antes de proceder a la reserva.";
$L1["046"] = "Descripción";
$L1["047"] = "Confirmación de reserva";
$L1["048"] = "Servicios del hotel";
$L1["049"] = "Hotel Recreación";
$L1["050"] = "ocupación";
$L1["051"] = "left";
$L1["052"] = "Más detalles";
$L1["053"] = "Instalaciones adicionales";
$L1["054"] = "Especiales de habitación";
$L1["055"] = "Comodidades de la habitación";
$L1["056"] = "Hoteles";
$L1["057"] = "Términos y Condiciones";
$L1["058"] = "Basado en";
$L1["059"] = "ahora";
$L1["060"] = "Total de Noches";
$L1["061"] = "¿Necesita ayuda?";
$L1["062"] = "Volver a casa";
$L1["063"] = "Usted puede también tener gusto";
$L1["064"] = "Ver Mapa";
$L1["065"] = "Continuar";
$L1["066"] = "No hay resultados !!";
$L1["067"] = "Ver en mapa";
$L1["068"] = "Quién";
$L1["069"] = "grado";
$L1["070"] = "Precio";
$L1["071"] = "Hoteles Tipo";
$L1["072"] = "Mis reservas";
$L1["073"] = "Mi Perfil";
$L1["074"] = "Lista de Deseos";
$L1["075"] = "Mis reservas";
$L1["076"] = "factura";
$L1["077"] = "Item";
$L1["078"] = "Total";
$L1["079"] = "Fecha de Vencimiento";
$L1["080"] = "Estado";
$L1["081"] = "Pagado";
$L1["082"] = "sin pagar";
$L1["083"] = "Agregar Comentario";
$L1["084"] = "Añadir Comentario para";
$L1["085"] = "Una vez comentario añadido no se puede eliminar o actualizar";
$L1["086"] = "Enviar";
$L1["087"] = "Nada reserva embargo";
$L1["088"] = "Información Personal";
$L1["089"] = "Title";
$L1["090"] = "Nombre";
$L1["091"] = "Apellido";
$L1["092"] = "Teléfono";
$L1["093"] = "¿Dónde debemos enviar su confirmación";
$L1["094"] = "Enviar";
$L1["095"] = "Contraseña";
$L1["096"] = "Confirm Password";
$L1["097"] = "Tu Lugar";
$L1["098"] = "Dirección";
$L1["099"] = "Dirección 2";
$L1["0100"] = "Ciudad";
$L1["0101"] = "Estado";
$L1["0102"] = "Región";
$L1["0103"] = "Postal";
$L1["0104"] = "Código Postal";
$L1["0105"] = "País";
$L1["0106"] = "Update";
$L1["0107"] = "Lista de Deseos";
$L1["0108"] = "Eliminar";
$L1["0109"] = "Vista previa";
$L1["0110"] = "No hay lista de deseos Artículo encontrado";
$L1["0111"] = "Seleccione Sí para recibir boletines de noticias!";
$L1["0112"] = "Olvidar la contraseña";
$L1["0113"] = "Recuérdame";
$L1["0114"] = "Reset";
$L1["0115"] = "Sign Up";
$L1["0116"] = "Factura A cargo";
$L1["0117"] = "Pagar ahora";
$L1["0118"] = "Obtener Descuento";
$L1["0119"] = "Vuela desde";
$L1["0120"] = "Llegada a";
$L1["0121"] = "Resumen de Orden";
$L1["0122"] = "Nights";
$L1["0123"] = "Cantidad";
$L1["0124"] = "Total";
$L1["0125"] = "Cuota de Transacción";
$L1["0126"] = "Depositar ahora";
$L1["0127"] = "Detalles de Reservas";
$L1["0128"] = "Confirmar Reserva Detalles";
$L1["0129"] = "Sitio Web";
$L1["0130"] = "móvil";
$L1["0131"] = "cordiales";
$L1["0132"] = "Equipo de servicio";
$L1["0133"] = "Página Goto";
$L1["0134"] = "Filtrar";
$L1["0135"] = "Confirmado";
$L1["0136"] = "Filtrar por";
$L1["0137"] = "Estrella";
$L1["0138"] = "Precios";
$L1["0139"] = "tipo Acomodation";
$L1["0140"] = "Score";
$L1["0141"] = "medio / noche";
$L1["0142"] = "Reservar";
$L1["0143"] = "Mostrar en un mapa";
$L1["0144"] = "Max ocupación";
$L1["0145"] = "Reservar";
$L1["0146"] = "Mi cuenta";
$L1["0147"] = "Términos de uso";
$L1["0148"] = "Política y normas";
$L1["0149"] = "Cancelación y reembolso";
$L1["0150"] = "Total";
$L1["0151"] = "reserva";
$L1["0152"] = "No disponible";
$L1["0153"] = "Impuestos e IVA";
$L1["0154"] = "Pagar por";
$L1["0155"] = "Seleccionar habitación";
$L1["0156"] = "Extras";
$L1["0157"] = "Publicidad";
$L1["0158"] = "Seleccione";
$L1["0159"] = "Seleccione Forma de pago";
$L1["0160"] = "Ver factura";
$L1["0161"] = "Página Goto";
$L1["0162"] = "En breve recibirás un correo de confirmación";
$L1["0163"] = "El pago procesado con éxito";
$L1["0164"] = "Pagado";
$L1["0165"] = "Debido";
$L1["0166"] = "código promocional";
$L1["0167"] = "libro como invitado";
$L1["0168"] = "Ingresar";
$L1["0169"] = "Crear una cuenta";
$L1["0170"] = "Información Personal";
$L1["0171"] = "Nombre";
$L1["0172"] = "Apellido";
$L1["0173"] = "número de móvil";
$L1["0174"] = "¿Dónde debemos enviar su confirmación?";
$L1["0175"] = "Confirmar";
$L1["0176"] = "Solicitudes especiales";
$L1["0177"] = "Detalles";
$L1["0178"] = "Notas / Las solicitudes adicionales";
$L1["0179"] = "Espere por favor ...";
$L1["0180"] = "Redirigir a su factura.";
$L1["0181"] = "Room Size";
$L1["0182"] = "Size";
$L1["0183"] = "El tamaño del baño";
$L1["0184"] = "Servicios";
$L1["0185"] = "Nuestra opinión";
$L1["0186"] = "Instalaciones adicionales";
$L1["0187"] = "Remember Me";
$L1["0188"] = "descripción de servicios";
$L1["0189"] = "rápida";
$L1["0190"] = "ok";
$L1["0191"] = "Filtros";
$L1["0192"] = "AGREGAR SU NEGOCIO";
$L1["0193"] = "creciendo exponencialmente";
$L1["0194"] = "Revisión de";
$L1["0195"] = "Puntuación de opinión";
$L1["0196"] = "Ordenar por";
$L1["0197"] = "Refinar fechas";
$L1["0198"] = "Cantidad de pasajeros";
$L1["0199"] = "Cantidad de equipaje";
$L1["0200"] = "Puertas Cantidad";
$L1["0201"] = "Engranaje de transmisión";
$L1["0202"] = "Aire Acondicionado";
$L1["0203"] = "GRATIS";
$L1["0204"] = "Cancelaciones";
$L1["0205"] = "Enmienda";
$L1["0206"] = "kilometraje ilimitado";
$L1["0207"] = "recogida en el aeropuerto";
$L1["0208"] = "Mejor precio garantizado";
$L1["0209"] = "ciudad, región, distrito o coches";
$L1["0210"] = "Pick up";
$L1["0211"] = "Drop Off";
$L1["0212"] = "Pasajeros";
$L1["0213"] = "Doors";
$L1["0214"] = "Tipo de Auto";
$L1["0215"] = "Tipo Pickup";
$L1["0216"] = "Precios";
$L1["0217"] = "Filtro";
$L1["0218"] = "Inicio";
$L1["0219"] = "Fin";
$L1["0220"] = "Tipo de paquete";
$L1["0221"] = "Tour Categoría";
$L1["0222"] = "Tipo de Tour";
$L1["0223"] = "Fechas de Tour";
$L1["0224"] = "Fechas Duración";
$L1["0225"] = "Inicio";
$L1["0226"] = "Fin";
$L1["0227"] = "Número de habitaciones";
$L1["0228"] = "Visitar";
$L1["0229"] = "Fecha de Vencimiento";
$L1["0230"] = "Clasificación de los viajeros";
$L1["0231"] = "Tipo de Auto";
$L1["0232"] = "Seleccionar fechas";
$L1["0233"] = "Hecho";
$L1["0234"] = "Cerrar";
$L1["0235"] = "Publicidad";
$L1["0236"] = "Cuenta Login";
$L1["0237"] = "Sign Up";
$L1["0238"] = "SELECT HOTEL";
$L1["0239"] = "DETALLES DE RESERVA";
$L1["0240"] = "ÉXITO DE RESERVAS";
$L1["0241"] = "Registro de Proveedores";
$L1["0242"] = "Imagen";
$L1["0243"] = "Proveedor";
$L1["0244"] = "Has registrado correctamente vez que su cuenta es aprobada, usted recibirá más detalles.";
$L1["0245"] = "Por noche";
$L1["0246"] = "Tipos de habitación";
$L1["0247"] = "reserva";
$L1["0248"] = "Información general";
$L1["0249"] = "Servicios";
$L1["0250"] = "Navegar";
$L1["0251"] = "disponibilidad";
$L1["0252"] = "Disponible";
$L1["0253"] = "pasado";
$L1["0254"] = "Lugar";
$L1["0255"] = "Dirección";
$L1["0256"] = "número de teléfono";
$L1["0257"] = "horario comercial";
$L1["0258"] = "Día";
$L1["0259"] = "Tiempo";
$L1["0260"] = "Por favor, rellene el siguiente formulario y nos pondremos en contacto con usted tan pronto como sea posible ¿Necesitas respuestas rápidas Llámenos al número dado.?.";
$L1["0261"] = "Asunto";
$L1["0262"] = "Mensaje";
$L1["0263"] = "Enviar";
$L1["0264"] = "Addon / Servicios";
$L1["0265"] = "Opciones de pago";
$L1["0266"] = "Entrar con Facebook";
$L1["0267"] = "404";
$L1["0268"] = "Oops !! Página no encontrada";
$L1["0269"] = "Válida Hasta";
$L1["0270"] = "Contáctenos";
$L1["0271"] = "Tour";
$L1["0272"] = "Ver Mapa";
$L1["0273"] = "De";
$L1["0274"] = "A";
$L1["0275"] = "Days";
$L1["0276"] = "Nights";
$L1["0277"] = "Seleccionar Acomoda";
$L1["0278"] = "Tipo de paquete";
$L1["0279"] = "Tours";
$L1["0280"] = "inclusiones";
$L1["0281"] = "Exclusiones";
$L1["0282"] = "infantil";
$L1["0283"] = "Buscar ¿Qué?";
$L1["0284"] = "Búsqueda rápida";
$L1["0285"] = "Últimos mensajes";
$L1["0286"] = "Leer más";
$L1["0287"] = "Mensajes populares";
$L1["0288"] = "Categorías y Mensajes";
$L1["0289"] = "Si quieres puedes";
$L1["0290"] = "Hoteles relacionados";
$L1["0291"] = "Resultados de la búsqueda";
$L1["0292"] = "Mensajes de la categoría";
$L1["0293"] = "¿Por qué reservar con nosotros?";
$L1["0294"] = "Atención al cliente";
$L1["0295"] = "pagos garantizados con";
$L1["0296"] = "Configuración";
$L1["0297"] = "Ver más Ofertas";
$L1["0298"] = "o";
$L1["0299"] = "De / por la noche";
$L1["0300"] = "Tipos Hotel";
$L1["0301"] = "Precios";
$L1["0302"] = "localizaciones múltiples encontrado";
$L1["0303"] = "Clasificación";
$L1["0304"] = "GRATIS";
$L1["0305"] = "Necesitas una cuenta?";
$L1["0306"] = "CONFIRMAR ESTA DE RESERVAS";
$L1["0307"] = "Hola";
$L1["0308"] = "Reserve en línea o llámenos:";
$L1["0309"] = "no reembolsable";
$L1["0310"] = "El correo electrónico no coincide con el correo electrónico de confirmación";
$L1["0311"] = "El campo Correo electrónico debe contener una dirección válida de correo electrónico";
$L1["0312"] = "Se requiere";
$L1["0313"] = "El correo electrónico ya existe";
$L1["0314"] = "Nombre";
$L1["0315"] = "Apellido";
$L1["0316"] = "Número de tarjeta";
$L1["0317"] = "Jan";
$L1["0318"] = "febrero";
$L1["0319"] = "Mar";
$L1["0320"] = "abril";
$L1["0321"] = "mayo";
$L1["0322"] = "Junio";
$L1["0323"] = "Julio";
$L1["0324"] = "agosto";
$L1["0325"] = "septiembre";
$L1["0326"] = "octubre";
$L1["0327"] = "noviembre";
$L1["0328"] = "diciembre";
$L1["0329"] = "Fecha de caducidad";
$L1["0330"] = "Tipo de tarjeta";
$L1["0331"] = "Tarjeta CVV";
$L1["0332"] = "Habitación Total";
$L1["0333"] = "Impuesto";
$L1["0334"] = "Total";
$L1["0335"] = "Reserva completa";
$L1["0336"] = "Datos de la reserva fueron enviados a su correo electrónico.";
$L1["0337"] = "Escribe una crítica";
$L1["0338"] = "Oferta Especial";
$L1["0339"] = "Ver detalles";
$L1["0340"] = "Tasa";
$L1["0341"] = "destacado";
$L1["0342"] = "Trusted propiedad";
$L1["0343"] = "El mejor precio";
$L1["0344"] = "reembolsable";
$L1["0345"] = "Pagar a la llegada";
$L1["0346"] = "Cancelar";
$L1["0347"] = "Cancelado";
$L1["0348"] = "Ver factura";
$L1["0349"] = "Yooah!";
$L1["0350"] = "Nombre";
$L1["0351"] = "Las mejores tarifas";
$L1["0352"] = "Nuestro objetivo es ofrecer las tarifas más bajas posible";
$L1["0353"] = "Hoteles con mayor puntuación";
$L1["0354"] = "TOP CLIENTE CLASIFICADO";
$L1["0355"] = "Datos bancarios";
$L1["0356"] = "Distancias";
$L1["0357"] = "Aeropuerto";
$L1["0358"] = "Ciudad";
$L1["0359"] = "mercado";
$L1["0360"] = "Hospital";
$L1["0361"] = "Subsidio";
$L1["0362"] = "admiten";
$L1["0363"] = "Sí";
$L1["0364"] = "No";
$L1["0365"] = "Parking";
$L1["0366"] = "Fumar";
$L1["0367"] = "potable";
$L1["0368"] = "¿Por qué nosotros?";
$L1["0369"] = "TOUR principal destino";
$L1["0370"] = "Km";
$L1["0371"] = "Nota por favor";
$L1["0372"] = "Habitaciones disponibles";
$L1["0373"] = "Max";
$L1["0374"] = "Nº habitaciones";
$L1["0375"] = "Precio para";
$L1["0376"] = "Imágenes";
$L1["0377"] = "Seleccione Forma de pago";
$L1["0378"] = "Buscar el mejor precio en hoteles, vuelos, alquiler de coches y Tours en el mejor";
$L1["0379"] = "Buscar";
$L1["0380"] = "Introduzca su destino en la barra de búsqueda Seleccione las fechas y número de personas, y de Búsqueda.";
$L1["0381"] = "Comparar";
$L1["0382"] = ". Buscar hoteles por estrellas, precio, ubicación o instalaciones Siempre tenemos la estancia perfecta para usted al mejor precio";
$L1["0383"] = "Encuentra las tarifas más bajas";
$L1["0384"] = "Buscar más de 200k Listados";
$L1["0385"] = "Buscar cientos de aerolíneas";
$L1["0386"] = "Los mejores precios para su reserva";
$L1["0387"] = "Ahorre el dinero en su alquiler";
$L1["0388"] = "Reserva ahora y paga después!";
$L1["0389"] = "En general";
$L1["0390"] = "Su";
$L1["0391"] = "su opinión";
$L1["0392"] = "Enviar un e-mail";
$L1["0393"] = "Sitio Web Hotel";
$L1["0394"] = "Leer Comentarios";
$L1["0395"] = "Comparte tu experiencia";
$L1["0396"] = "opiniones";
$L1["0397"] = ". Simplemente haga clic en libro ahora Usted puede optar por pagar en línea o posterior a su llegada";
$L1["0398"] = "Código de Reservas";
$L1["0399"] = "Orden";
$L1["0400"] = "de";
$L1["0401"] = "Evaluaciones";
$L1["0402"] = "Blog News";
$L1["0403"] = "Introduzca su dirección de correo electrónico";
$L1["0404"] = "Recibir ofertas de todas nuestras principales ofertas";
$L1["0405"] = "Hotel";
$L1["0406"] = "Check";
$L1["0407"] = "Pagos";
$L1["0408"] = "Subtotal";
$L1["0409"] = "Su estado de reserva es";
$L1["0410"] = "Datos de la reserva fueron enviados a";
$L1["0411"] = "Resumen de la reserva";
$L1["0412"] = "Precio medio por noche";
$L1["0413"] = "Su reserva quedará pendiente hasta que pague la tasa de depósito.";
$L1["0414"] = "Número de contacto";
$L1["0415"] = "Usted puede entrar cualquier nota o información adicional que desea incluir con su pedido aquí ...";
$L1["0416"] = "Al hacer clic para completar esta reserva que reconozco que he leído y acepto las Reglas y Restricciones";
$L1["0417"] = "Datos bancarios";
$L1["0418"] = "No hay habitaciones disponibles !!!";
$L1["0419"] = "Proceda";
$L1["0420"] = "Por favor, seleccione las fechas válidas";
$L1["0421"] = "Anterior";
$L1["0422"] = "Siguiente";
$L1["0423"] = "Arriba de la página";
$L1["0424"] = "Sign in";
$L1["0425"] = "Correo no encontrado";
$L1["0426"] = "Por favor verifique correo electrónico";
$L1["0427"] = "Redirigir Espere por favor ...";
$L1["0428"] = "Camas Extra";
$L1["0429"] = "Camas Extras Cargos";
$L1["0430"] = "Añadir Extras";
$L1["0431"] = "Algo bebé equivocado por favor intente de nuevo.";
$L1["0432"] = "Operaciones!";
$L1["0433"] = "para confirmar su reserva!";
$L1["0434"] = "Número";
$L1["0435"] = "Room";
$L1["0436"] = "¿Seguro que desea eliminar de la lista de deseos?";
$L1["0437"] = "¿Desea añadir a lista de deseos?";
$L1["0438"] = "Llame ahora";
$L1["0439"] = "Contacto";
$L1["0440"] = "Days";
$L1["0441"] = "Horas";
$L1["0442"] = "Minutos";
$L1["0443"] = "segundos";
$L1["0444"] = "Gracias por ponerse en contacto";
$L1["0445"] = "reservados";
$L1["0446"] = "Invitados";
$L1["0447"] = "Seleccionar ubicación";
$L1["0448"] = "El correo electrónico del usuario";
$L1["0449"] = "Sitio Web del usuario";
$L1["0450"] = "No";
$L1["0451"] = "Tours destacados";
$L1["0452"] = "Top Rated Tours";
$L1["0453"] = "Tours relacionados";
$L1["0454"] = "Cambiar la fecha";
$L1["0455"] = "Número Máximo de Adultos superó para esta gira";
$L1["0456"] = "Máximo Número de niños superada para esta gira";
$L1["0457"] = "Número Máximo de Infantes superó para esta gira";
$L1["0458"] = "Instrucciones Checkin";
$L1["0459"] = "Información de pago";
$L1["0460"] = "Dirección de facturación";
$L1["0461"] = "El pago total se cargará a su tarjeta de crédito al reservar este hotel. Por favor, tenga en cuenta que su banco puede convertir el pago a su moneda local y le cobran una tarifa de conversión adicional. Esto significa que el cantidad que usted ve en su crédito o estado de cuenta bancaria puede ser en su moneda local y, por tanto, una figura diferente que el precio total mostrado anteriormente. Si usted tiene alguna pregunta sobre esta cuota o el tipo de cambio aplicado a su reserva, por favor póngase en contacto con su banco. ";
$L1["0462"] = "Notificaciones y cargos";
$L1["0463"] = "Opciones de reservación";
$L1["0464"] = "Preferimos hacer login para que pueda ver su historial de reserva previa y que nos ayuda a proporcionarle mejores recomendaciones";
$L1["0465"] = "Ver más hoteles";
$L1["0466"] = "Ver Más Tours";
$L1["0467"] = "Todo Incluido";
$L1["0468"] = "Suite";
$L1["0469"] = "complejo";
$L1["0470"] = "Alquiler de vacaciones / Condominio";
$L1["0471"] = "Bed & Breakfast";
$L1["0472"] = "Salir";
$L1["0473"] = "Volver";
$L1["0474"] = "Debe confirmar su reserva de lo contrario se cancelará, por favor póngase en contacto con nosotros para más información.";
$L1["0475"] = "Acerca de";
$L1["0476"] = "por teléfono";
$L1["0477"] = "LIBRO";
$L1["0478"] = "Tipos de propiedad";
$L1["0479"] = "Gracias por ponerse en contacto";
$L1["0480"] = "on";
$L1["0481"] = "Buscar la mejor guía de viajes y aritcles";
$L1["0482"] = "Por favor, Ingresar para agregar a la lista de deseos.";
$L1["0483"] = "Seguro que quieres pagar a la llegada ?";
$L1["0484"] = "Seleccionar país";
$L1["0485"] = "Usted ya ha publicado una opinión";
$L1["0486"] = "Comentario publicado con éxito";
$L1["0487"] = "suscrito con éxito";
$L1["0488"] = "Ya suscrito";
$L1["0489"] = "no suscribirse con éxito";
$L1["0490"] = "Coches destacados";
$L1["0491"] = "Top Rated Coches";
$L1["0492"] = "Ver Más Autos";
$L1["0493"] = "Coches Relacionados";
$L1["0494"] = "ayuda a hacer crecer su negocio";
$L1["0495"] = "Llegar a los más de 55 mil viajeros";
$L1["0496"] = "Recibe consejos de ventas personalizadas";
$L1["0497"] = "Recibe reservas directas . hombres sin medias!";
$L1["0498"] = "Escuchar y obtener puntos de vista no hay llamadas de los clientes";
$L1["0499"] = "No hay cuota de escucha. o coste oculto";
$L1["0500"] = "Obtener acceso a panel de control en tiempo real";
$L1["0501"] = "Presentar Negocio";
$L1["0502"] = "Fácil proceso de registro";
$L1["0503"] = "Register & add your business";
$L1["0504"] = "Mostramos su propiedad de una manera que sea relevante para los clientes de todo el mundo , hasta en 41 idiomas . También comercializamos sus anuncios en los motores de búsqueda como Google , Bing y Yahoo para ayudarle a vender más habitaciones y aumentar los ingresos!";
$L1["0505"] = "Consigue verificado";
$L1["0506"] = "Verifique su negocio con nosotros";
$L1["0507"] = "Verifique su negocio con nosotros y obtener un enorme potencial de nuestro tráfico donde se obtiene más ingresos y mejores negocios en línea";
$L1["0508"] = "Acceder";
$L1["0509"] = "Administrar su tablero de instrumentos en tiempo real";
$L1["0510"] = "Obtener acceso a su panel de control en tiempo real y empezar a gestionar sus contenidos tales como descripción, título , imágenes, precios y fechas avaialability .";
$L1["0511"] = "Envía tu negocio ahora";
$L1["0512"] = "cupón de descuento se ha aplicado por favor haga clic en libro ahora para ver el precio con descuento en la página de la factura.";
$L1["0513"] = "Cupón no válido";
$L1["0514"] = "Un código promocional es la clave para canjear descuentos especiales. Para redimir, ingrese su código promocional y aplicaremos el descuento al costo total en su página de la factura cuando usted paga. Vales de descuento sólo se pueden usar una vez con cada reserva, y son buenos sólo hasta el monto total de su reserva (incluyendo cargo por servicio e impuestos) o la cantidad código promocional, que sea menor";
$L1["0515"] = "Qué es un código promocional?";
$L1["0516"] = "el descuento se mostrará en la página de la factura";
$L1["0517"] = "Aplicar cupón";
$L1["0518"] = "Descuento Aplicada";
$L1["0519"] = "expirado";
$L1["0520"] = "Este cupón no es aplicable en este caso";
$L1["0521"] = "Confort Informations";
$L1["0522"] = "Huésped";
$L1["0523"] = "Sin pasaporte.";
$L1["0524"] = "Años";
$L1["0525"] = "Descarga la aplicación";
$L1["0526"] = "Mejores Lugares";
$L1["0527"] = "Ofrecemos la opción de más de 257.000 localidades en más de 200 países. Nuestros comentarios le ayudarán a encontrar la mejor oferta en el lugar correcto .";
$L1["0528"] = "2500+ Clientes";
$L1["0529"] = "Creemos que un buen servicio es la clave de toda empresa exitosa . por eso trabajamos duro para servirle con el mejor precio , la ubicación y el apoyo .";
$L1["0530"] = "Soporte 24/7";
$L1["0531"] = "Está buscando en busca de familiares o viaje de negocios ? reservar en línea o ponerse en contacto con nuestra línea telefónica 24 horas , si usted prefiere hablar con alguien .";
$L1["0532"] = "Cómo empezar?";
$L1["0533"] = "Siga los siguientes pasos simples para empezar a reservar su viaje ahora!";
$L1["0534"] = "Búsqueda de su viaje";
$L1["0535"] = "Lee los detalles y las revisiones";
$L1["0536"] = "Reservar y pagar en cualquier momento .";
$L1["0537"] = "Hemos cargado su tarjeta de crédito para el pago total de la reserva.";
$L1["0538"] = "incluyendo los impuestos";
$L1["0539"] = "Número de confirmación";
$L1["0540"] = "Itinerario ID";
$L1["0541"] = "Cargos y tasas de recuperación";
$L1["0542"] = "Precio por 1 noche (s) incluyendo impuestos y tasas";
$L1["0543"] = "Por favor, lugares de recogida y recepción de boletas";
$L1["0544"] = "lugares de recogida y recepción de boletas no debe ser el mismo";
$L1["0545"] = "Detalles del cliente";
$L1["0546"] = "reservado a través de";
$L1["0547"] = "Fecha para registrarse";
$L1["0548"] = "Detalles de contacto";
$L1["0549"] = "Coche no disponible.";