$(document).ready(function() {
	 $(".datepicker").datepicker();
	$('tr:odd').addClass('odd');
	
	/****** Bouton affichage menu ******/	
	$(".navButton a").click(function(e){
		e.preventDefault();
		$('.menuList').slideToggle();
	});
	
	/**** Select ****/
	$(function () {
		$(".selectStyled").selectbox();
	});
	
	/*** Radio et checkBox Personnalisé ***/
	
	$('.checkStyle').on('ifCreated ifClicked ifChanged ifChecked ifUnchecked ifDisabled ifEnabled ifDestroyed', function(event){
		}).iCheck({
			checkboxClass: 'icheckbox',
			radioClass: 'iradio',
			increaseArea: '20%'
	});
	/***********page vosreservatiions COMMENT*************/
	
	$(".afficher_all_comment").click(function(e){
		e.preventDefault();
		var clone=$(this).parent('.ligne_reservationbit');
		if ($(this).parent('.ligne_reservationbit').parent('.pi-tab-pane').find('.all_comment_page').is(":hidden")) {
			$(this).parent('.ligne_reservationbit').parent('.pi-tab-pane').find('.all_comment_page').slideDown();
			$(this).find('.affiche_nslide').css("display", "none");
			$(this).find('.affiche_qslide').css("display", "block");
			$(this).parent('.ligne_reservationbit').parent('.pi-tab-pane').find('.all_comment_page').after(clone);
			
		}
		else{
			$(this).parent('.ligne_reservationbit').parent('.pi-tab-pane').find('.all_comment_page').slideUp();
			$(this).find('.affiche_nslide').css("display", "block");
			$(this).find('.affiche_qslide').css("display", "none");
			$(this).parent('.ligne_reservationbit').parent('.pi-tab-pane').find('.all_comment_page').before(clone);
		}
	});
	/****** Hover galerie photo ******/	
	$(".photoContainer").hover(function(){
		$(this).find(".photoLink").fadeToggle();
	});
	/*$('.fancybox').fancybox();*/
	$(".content_add_photo .photoContent").hover(function(){
		$(this).find(".photoCLink").fadeToggle();
	});
	/****** Pop Up Statistique ******/	
	$(".detailStat a").click(function(e){
		e.preventDefault();
		$('.popUp_wrapper').fadeIn();
	});
	$(".popUp_close").click(function(e){
		e.preventDefault();
		$('.popUp_wrapper').fadeOut();
	});
	/****** Synthèse Tarif tablo ******/	
	$(".titre_violet_synthese").each(function(e) {
       $(this).toggle(function(e){
		   
		e.preventDefault();
		if($(this).parent('td').parent('tr').next('.prix_tarif').is(':hidden')){
			$(this).addClass("px_open");
			$(this).parent('td').parent('tr').next('.prix_tarif').slideDown();
		}
		else{
			$(this).removeClass("px_open");
			$(this).parent('td').parent('tr').next('.prix_tarif').slideUp();	
		}
	}, function(e){
		if($(this).parent('td').parent('tr').next('.prix_tarif').is(':visible')){
			$(this).removeClass("px_open");
			$(this).parent('td').parent('tr').next('.prix_tarif').slideUp();
	}
		else{
			$(this).addClass("px_open");
			$(this).parent('td').parent('tr').next('.prix_tarif').slideDown();	
		}
	});
    });
	
	
	/*******affichage reponse avis********/
	$(".votre_avislien").click(function(e){
		e.preventDefault();
		if ($(this).parent('.action_avis_mois').next('.box_votre_reponse').is(":hidden")) {
			$(this).parent('.action_avis_mois').next('.box_votre_reponse').slideDown();
			$(this).find('.affiche_nslide').css("display", "none");
			$(this).find('.affiche_qslide').css("display", "block");
		}
		else{
		$(this).parent('.action_avis_mois').next('.box_votre_reponse').slideUp();
		$(this).find('.affiche_nslide').css("display", "block");
		$(this).find('.affiche_qslide').css("display", "none");	
		}
	});
	$(".lien_souligner_detail").click(function(e){
		e.preventDefault();
		if ($(this).next('.detail_note_avis').is(":hidden")) {
			$(this).next('.detail_note_avis').slideDown();
			$(this).find('.affiche_nslide').css("display", "none");
			$(this).find('.affiche_qslide').css("display", "block");
		}
		else{
		$(this).next('.detail_note_avis').slideUp();
		$(this).find('.affiche_nslide').css("display", "block");
		$(this).find('.affiche_qslide').css("display", "none");	
		}
	});
	
/****** Script pop up ******/	
	$("a.popUpfermerdispo").click(function(e){
		e.preventDefault();
		$('.popUpOverlay').hide();
		$('#pop_up_fermer_dispo').fadeIn();
	});
	$("a.popUpModifplanning").click(function(e){
		e.preventDefault();
		$('.popUpOverlay').hide();
		$('#pop_up_modif_planning').fadeIn();
	});
	$("a.popUpModifplanning2").click(function(e){
		e.preventDefault();
		$('.popUpOverlay').hide();
		$('#pop_up_modif_planning2').fadeIn();
	});
	
	$(".close_popup").click(function(e){
		e.preventDefault();
		$('.popUpOverlay').fadeOut();
	});
	
});